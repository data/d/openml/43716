# OpenML dataset: Football-Matches-of-Spanish-League

https://www.openml.org/d/43716

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Data Set with the football matches of the Spanish league of the 1st and 2nd division from the 1970-71 to 2016-17 season, has been created with the aim of opening a line of research in the Machine Learning, for the prediction of results (1X2) of football matches.
Content
This file contains information about a football matches with the follow features:
4808,1977-78,1,8,Rayo Vallecano,Real Madrid,3,2,30/10/1977,247014000


id (4808): Unique identifier of football match
season (1977-78): Season in which the match was played
division (1): Divisin in which the match was played (1st '1', 2nd '2')
round (8): round in which the match was played
localTeam (Rayo Vallecano): Local Team name
visitorTeam (Real Madrid): Visitor Team name
localGoals (3): Goals scored by the local team
visitorGoals (2): Goals scored by the visitor team
fecha (30/10/1977): Date in which the match was played
date (247014000): Timestamp in which the match was played

Acknowledgements
Scraping made from:

http://www.bdfutbol.com
http://www.resultados-futbol.com

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43716) of an [OpenML dataset](https://www.openml.org/d/43716). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43716/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43716/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43716/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

